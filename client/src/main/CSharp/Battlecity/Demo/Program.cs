/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2021 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
﻿using System;
using System.Collections.Generic;
 using System.IO;
 using System.Linq;
using System.Text;
using System.Threading.Tasks;
 using System.Xml.Serialization;
 using API.Data;
 using API.Misc;
 using Battlecity.API;

namespace Demo
{
    class Program
    {
        static string serverURL = "https://epam-botchallenge.com/codenjoy-contest/board/player/wux58x26b0cxul8t7p3n?code=7989235335852510663";
        static string testServerURL = "https://practice.epam-botchallenge.com/codenjoy-contest/board/player/ynxqnlthv158udes5k4h?code=6495102902680183443";

        static void Main(string[] args)
        {
	        try
	        {
		        Console.OutputEncoding = System.Text.Encoding.Unicode;
		        Console.SetWindowSize(Console.LargestWindowWidth - 3, Console.LargestWindowHeight - 3);


		        // Creating custom AI client
		        var parameters = ReadParameters();
				var bot = new YourSolver(serverURL, parameters);

		        // Starting thread with playing game
		        Task.Run(bot.Play);
		        Console.ReadKey();

		        // On any key - asking AI client to stop.
		        bot.InitiateExit();
}
	        finally
	        {
				File.WriteAllText("Log.txt", Log.ToString());
	        }
        }

        private static Parameters ReadParameters()
        {
	        XmlSerializer serializer =
		        new XmlSerializer(typeof(Parameters));

	        using (Stream reader = new FileStream("Parameters.xml", FileMode.Open))
	        {
		        // Call the Deserialize method to restore the object's state.
		        return (Parameters)serializer.Deserialize(reader);
	        }
        }
    }
}

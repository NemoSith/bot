﻿using API.Data;
using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = System.Text.Encoding.Unicode;
			var map = new Map(new Parameters());
			map.SetPlayerTankPosition(new Battlecity.API.Point(32, 32));
			map.Grid.SetPlayer(map.PlayerTank);
			
			var q = Utilities.MeasureOperationTime<List<Tile>>(() => PathFinder.FindPath(map.Grid, map.Grid.GetPlayer().Location, map.Grid.GetTile(10, 6)), "FindPath");
			foreach (var w in q)
			{
				map.SetPathTile(w.Location);
			}

			Console.Clear();
			Console.SetCursorPosition(0, 0);
			//Console.WriteLine(board.ToString());
			Console.WriteLine(map.ToString());
			//Console.WriteLine("\n"+string.Join($"  ", q.Select(x => x.Location.ToString())));
			Console.WriteLine("\n" + Log.ToString());


			Console.ReadKey();
		}
	}
}

﻿using Battlecity.API;

namespace API.CleverStrategies
{
	public class StrategyResult
	{
		public Direction FirstAction { get; set; }
		public Direction SecondAction { get; set; }
		public Point MovementDestination { get; set; }
	}
}

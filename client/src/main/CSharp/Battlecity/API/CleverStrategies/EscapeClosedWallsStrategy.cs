﻿using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.CleverStrategies
{
	public class EscapeClosedWallsStrategy : MoveToActionStrategy
	{
		public override StrategyResult Apply()
		{
			var result = new StrategyResult();
			var playerLoc = Map.PlayerTank.Location;
			if (SelectedDestination != null)
			{
				//check if enemy destroyed
				if (Map.Grid.GetTile(SelectedDestination.Location.X, SelectedDestination.Location.Y).OriginalChar == 'Ѡ')
				{
					Log.Append($"Previous enemy destroyed.");
					SelectedDestination = null;
				}
				else//get new movement point to selected enemy
				{
					var possibleNewLocations = SelectedDestination.Location.Neighbors();
					var possibleNewTiles = possibleNewLocations.Select(x => Map.Grid.GetTile(x.X, x.Y)).ToList();
					SelectedDestination = possibleNewTiles.FirstOrDefault(x => x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot);
				}
			}

			if (SelectedDestination == null)
			{
				var enemyClasters = DestinationSelector.DBSCAN(Map.Grid);
				var parameters = Map.Parameters;
				var minDistsToClaster = new List<Tuple<double, List<Tile>>>();
				foreach (var claster in enemyClasters)
				{
					var minDist = claster.Select(x => new { q = x, o = x.Location.Distance(playerLoc.Location) }).OrderBy(x => x.o);
					minDistsToClaster.Add(new Tuple<double, List<Tile>>(minDist.FirstOrDefault()?.o ?? double.MaxValue, claster));
				}

				var ordered = minDistsToClaster.OrderBy(x => x.Item2.Count).ThenBy(x => x.Item1);
				SelectedDestination = ordered.FirstOrDefault(x => x.Item1 < MaxDistToClaster)?.Item2.First() ?? ordered.FirstOrDefault().Item2.First();
			}

			var route = PathFinder.Models.PathFinder.FindPathThroughWalls(Map.Grid, playerLoc, SelectedDestination);
			if (route.Count > 1)
			{
				result.FirstAction = Utilities.GetDirection(playerLoc.Location, route[1].Location);
				if (route[1].IsObstacle)
					result.SecondAction = Map.PlayerTank.Shoot();

				result.MovementDestination = route[1].Location;
			}

			return result;
		}
	}
}

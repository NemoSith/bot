﻿using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System.Collections.Generic;
using System.Linq;

namespace API.CleverStrategies
{
	public class FightCloseEnemyStrategy : ICleverStrategy
	{
		private const int closeDist = 15;
		private Map Map;

		public int Priority { get; private set; }

		public void Init(int priority)
		{
			Priority = priority;
		}

		public void Update(Map map)
		{
			Map = map;
		}

		public StrategyResult Apply()
		{
			var result = new StrategyResult();
			var player = Map.PlayerTank;
			var closeEnemies = Map.Grid.GetEnemies().Select(x => new { tank = x, dist = x.Location.Location.Distance(player.Location.Location) }).OrderBy(x => x.dist).Where(x => x.dist < closeDist).ToList();
			if(closeEnemies.Any())
			{
				if(player.CanShoot)
				{
					Log.Append($"There are close enemies: \n{string.Join("\n", closeEnemies.Select(x => $"{x.tank} in {x.dist}"))}");
					var shootDecisions = new Dictionary<Direction, List<Tank>>
					{
						{ Direction.Left, new List<Tank>() },
						{ Direction.Right, new List<Tank>() },
						{ Direction.Up, new List<Tank>() },
						{ Direction.Down, new List<Tank>() }
					};
					var neighborEnemy = closeEnemies.FirstOrDefault(x => x.dist == 1);
					if (neighborEnemy != null)
					{
						Log.Append($"Shooting neighbor enemy: {neighborEnemy.tank}");
						var toEnemyDir = player.Location.Location.GetDirection1(neighborEnemy.tank.Location.Location);
						if(toEnemyDir != player.TurnDirection.GetDirection()) result.FirstAction = toEnemyDir;
						result.SecondAction = player.Shoot();
					}

					var sameVertice = closeEnemies
						.Select(x => x.tank)
						.Where(x => x.Location.Location.X == player.Location.Location.X || x.Location.Location.Y == player.Location.Location.Y)
						.ToList();
					foreach (var enemy in sameVertice)
					{
						var d = player.Location.Location.GetDirection1(enemy.Location.Location);
						shootDecisions[d].Add(enemy);
						Log.Append($"{enemy} to the {d}");
					}

					//Player tank looks at enemy
					//Player will shoot and move to less enemies side
					if (player.TurnDirection != TurnDirection.None && player.TurnDirection != TurnDirection.Unknown && shootDecisions[player.TurnDirection.GetDirection()].Any())
					{
						Log.Append($"Looking at enemy, shoot");
						result.FirstAction = player.Shoot();
					}
					else
					{
						var stayingSameVertice = new List<Direction>();
						foreach(var decision in shootDecisions)
						{
							if (decision.Value.Where(x => x.TurnDirection.GetDirection() == decision.Key || x.TurnDirection.GetDirection().OpositDirection() == decision.Key).Any())
								stayingSameVertice.Add(decision.Key);
						}

						//Player tank take a look on enemy who looks at him, than shoot
						if (stayingSameVertice.Count > 0)
						{
							Log.Append($"Enemy will stay on its traectory, turn {stayingSameVertice[0]} and shoot");
							result.FirstAction = stayingSameVertice[0];
							result.SecondAction = player.Shoot();
						}
					}
				}
			}

			return result;
		}
	}
}

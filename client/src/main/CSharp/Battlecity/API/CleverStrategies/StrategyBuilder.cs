﻿using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.CleverStrategies
{
	public class StrategyBuilder
	{
		List<ICleverStrategy> cleverStrategies;
		Map Map;

		public StrategyBuilder(Dictionary<ICleverStrategy, int> strategies)
		{
			cleverStrategies = new List<ICleverStrategy>();
			foreach (var strategy in strategies)
			{
				cleverStrategies.Add(strategy.Key);
				strategy.Key.Init(strategy.Value);
			}

			cleverStrategies = cleverStrategies.OrderByDescending(x => x.Priority).ToList();
		}

		public void Update(Map map)
		{
			Map = map;
			foreach (var str in cleverStrategies)
			{
				str.Update(map);
			}
		}

		public KeyValuePair<Direction, Direction> Apply()
		{
			var frst = Direction.None;
			var scnd = Direction.None;
			var didShoot = false;
			var didMove = false;
			try
			{
				foreach (var str in cleverStrategies)
				{
					var decision = str.Apply();
					if (frst == Direction.None && decision.FirstAction != Direction.None)
					{
						if (!didShoot && decision.FirstAction == Direction.Act) { didShoot = true; frst = decision.FirstAction; }
						if (!didMove && decision.FirstAction != Direction.Act) { didMove = true; frst = decision.FirstAction; Map.PlayerTank.TurnDirection = frst.GetTurnDirection(); }
					}

					if (scnd == Direction.None && decision.SecondAction != Direction.None)
					{
						if (!didShoot && decision.SecondAction == Direction.Act) { didShoot = true; scnd = decision.SecondAction; }
						if (!didMove && decision.SecondAction != Direction.Act) { didMove = true; scnd = decision.SecondAction; Map.PlayerTank.TurnDirection = scnd.GetTurnDirection(); }
					}

					/*if(didShoot && didMove)
					{
						Log.Append($"Correcting move direction");
						if (frst == Direction.Act && scnd == Map.PlayerTank.TurnDirection.GetDirection()) scnd = Direction.None;
						if (scnd == Direction.Act && frst == Map.PlayerTank.TurnDirection.GetDirection()) frst = Direction.None;
					}*/

					if (frst != Direction.None && scnd != Direction.None)
					{
						break;
					}
				}

				if (frst == Direction.None && scnd != Direction.None) { frst = scnd; scnd = Direction.None; }
				Log.Append($"Action: {frst} then {scnd}");
			}
			catch(Exception e)
			{
				Log.Append(e.ToString());
				Log.Append(Map.ToString());
			}

			return new KeyValuePair<Direction, Direction>(frst, scnd);
		}
	}
}

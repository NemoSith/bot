﻿using API.PathFinder.Models;

namespace API.CleverStrategies
{
	public interface ICleverStrategy
	{
		int Priority { get; }
		void Init(int priority);
		void Update(Map map);
		StrategyResult Apply();
	}
}

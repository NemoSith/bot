﻿using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.CleverStrategies
{
	public class ShootPredictStrategy : ICleverStrategy
	{
		private const int BotCost = 50;
		private const int AiBotCost = 25;

		private const int CloseTileDist = 3;
		Map Map;
		public int Priority { get; private set; }

		public StrategyResult Apply()
		{
			var result = new StrategyResult();
			var player = Map.PlayerTank;
			if (player.CanShoot)
			{
				var enemies = Map.Grid.GetEnemies();
				ShootSameVerticeEnemy(player, enemies, result);
				if (result.FirstAction == Direction.None)
				{
					Log.Append("Did not shoot on same vrtice, will try to make predict shot.");
					ShootPredictDiagonalEnemy(player, enemies, result);
				}
			}

			Log.Append($"Shoot strategy result: [{result.FirstAction}, {result.SecondAction}]");
			return result;
		}

		private void ShootSameVerticeEnemy(Tank player, List<Tank> enemies, StrategyResult result)
		{
			var left = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Left, Map.Size)
					  .TakeWhile(x => x?.IsBulletObstacle == false);//Fix we should take while not obstacle for bullet 
																	//than we should iterate it to find first enemy
			var right = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Right, Map.Size)
				.TakeWhile(x => x?.IsBulletObstacle == false);
			var up = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Up, Map.Size)
				.TakeWhile(x => x?.IsBulletObstacle == false);
			var down = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Down, Map.Size)
				.TakeWhile(x => x?.IsBulletObstacle == false);

			var sameVerticeEnemies = new List<SameVerticeEnemy>();
			FindSameVerticeEnemies(left, enemies, sameVerticeEnemies);
			FindSameVerticeEnemies(right, enemies, sameVerticeEnemies);
			FindSameVerticeEnemies(up, enemies, sameVerticeEnemies);
			FindSameVerticeEnemies(down, enemies, sameVerticeEnemies);
			if (sameVerticeEnemies.Any())
			{
				Log.Append($"There are same veritces enemies: \n\t{string.Join("\n\t", sameVerticeEnemies.Select(x => $"{x.Tank} is on {x.DirectionFromPlayer} from Player"))}");
			}

			var closeEnemies = sameVerticeEnemies.Where(x => x.TilesCount < CloseTileDist).OrderBy(x => x.Tank.Type).ToList();
			var mostValuableCloseEnemy = closeEnemies.FirstOrDefault();
			if (mostValuableCloseEnemy != null)
			{
				Log.Append($"There is close enemy: {mostValuableCloseEnemy.Tank} on {mostValuableCloseEnemy.DirectionFromPlayer} from Player");
				ShootEnemy(mostValuableCloseEnemy.DirectionFromPlayer, result);
				return;
			}

			var stayOnLane = sameVerticeEnemies.Where(x => (x.DirectionFromPlayer == Direction.Left && (x.Tank.TurnDirection == TurnDirection.Left || x.Tank.TurnDirection == TurnDirection.Right))
					|| (x.DirectionFromPlayer == Direction.Right && (x.Tank.TurnDirection == TurnDirection.Left || x.Tank.TurnDirection == TurnDirection.Right))
					|| (x.DirectionFromPlayer == Direction.Up && (x.Tank.TurnDirection == TurnDirection.Up || x.Tank.TurnDirection == TurnDirection.Down))
					|| (x.DirectionFromPlayer == Direction.Down && (x.Tank.TurnDirection == TurnDirection.Up || x.Tank.TurnDirection == TurnDirection.Down)))
				.OrderBy(x => x.TilesCount)
				.ThenBy(x => x.Tank.Type)
				.ToList();
			mostValuableCloseEnemy = stayOnLane.FirstOrDefault();
			if (mostValuableCloseEnemy != null)
			{
				Log.Append($"There is enemy who moves in the same vertice: {mostValuableCloseEnemy.Tank} on {mostValuableCloseEnemy.DirectionFromPlayer} from Player");
				ShootEnemy(mostValuableCloseEnemy.DirectionFromPlayer, result);
			}
		}

		private void ShootPredictDiagonalEnemy(Tank player, List<Tank> enemies, StrategyResult result)
		{
			var left = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Left, 2).TakeWhile(x => x?.IsObstacle == false).ToList();
			var right = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Right, 2).TakeWhile(x => x?.IsObstacle == false).ToList();
			var up = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Up, 2).TakeWhile(x => x?.IsObstacle == false).ToList();
			var down = Map.Grid.GetCurTurnTilesOnSide(player.Location.Location, Direction.Down, 2).TakeWhile(x => x?.IsObstacle == false).ToList();

			var leftEnemies = left.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Up, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Down) || y?.GameObject == GameObject.PlayerBot))
				.Concat(left.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Down, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Up) || y?.GameObject == GameObject.PlayerBot)));
			var rightEnemies = right.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Up, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Down) || y?.GameObject == GameObject.PlayerBot))
				.Concat(right.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Down, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Up) || y?.GameObject == GameObject.PlayerBot)));
			var upEnemies = up.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Left, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Right) || y?.GameObject == GameObject.PlayerBot))
				.Concat(up.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Right, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Left) || y?.GameObject == GameObject.PlayerBot)));
			var downEnemies = down.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Left, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Right) || y?.GameObject == GameObject.PlayerBot))
				.Concat(down.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Right, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Left) || y?.GameObject == GameObject.PlayerBot)));

			#region logs
			if (leftEnemies.Any())
			{
				Log.Append($"There are some predictable enemies to the Left: \n\t{string.Join("\n\t", leftEnemies.Select(x => $"{x.GameObject} is on tile {x.Location} looking at {x.GameObjectDirection}"))}");
			}

			if (rightEnemies.Any())
			{
				Log.Append($"There are some predictable enemies to the Right: \n\t{string.Join("\n\t", rightEnemies.Select(x => $"{x.GameObject} is on tile {x.Location} looking at {x.GameObjectDirection}"))}");
			}

			if (upEnemies.Any())
			{
				Log.Append($"There are some predictable enemies to the Up: \n\t{string.Join("\n\t", upEnemies.Select(x => $"{x.GameObject} is on tile {x.Location} looking at {x.GameObjectDirection}"))}");
			}

			if (downEnemies.Any())
			{
				Log.Append($"There are some predictable enemies to the Down: \n\t{string.Join("\n\t", downEnemies.Select(x => $"{x.GameObject} is on tile {x.Location} looking at {x.GameObjectDirection}"))}");
			}
			#endregion

			var verticePointsCount = new Dictionary<Direction, int>
			{
				{ Direction.Left, leftEnemies.Sum(x=> x.GameObject == GameObject.PlayerBot ? BotCost : x.GameObject == GameObject.AiBot ? 25 : 0) },
				{ Direction.Right, rightEnemies.Sum(x=> x.GameObject == GameObject.PlayerBot ? BotCost : x.GameObject == GameObject.AiBot ? 25 : 0) },
				{ Direction.Up, upEnemies.Sum(x=> x.GameObject == GameObject.PlayerBot ? BotCost : x.GameObject == GameObject.AiBot ? 25 : 0) },
				{ Direction.Down, downEnemies.Sum(x=> x.GameObject == GameObject.PlayerBot ? BotCost : x.GameObject == GameObject.AiBot ? 25 : 0) }
			};

			var shootDir = verticePointsCount.OrderByDescending(x => x.Value).FirstOrDefault();
			if(shootDir.Value > 0)
			{
				var decision = ShootEnemy(shootDir.Key, result);
				Log.Append($"There is predictable target, going to {decision.Key} then {decision.Value}");
				result.FirstAction = decision.Key;
				result.SecondAction = decision.Value;
			}
			
			//var leftTop = left.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Up, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Down) || y?.GameObject == GameObject.PlayerBot));
			//var leftBot = left.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Down, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Up) || y?.GameObject == GameObject.PlayerBot));
			//var rightTop = right.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Up, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Down) || y?.GameObject == GameObject.PlayerBot));
			//var rightBot = right.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Down, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Up) || y?.GameObject == GameObject.PlayerBot));
			//var topLeft = up.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Left, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Right) || y?.GameObject == GameObject.PlayerBot));
			//var topRight = up.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Right, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Left) || y?.GameObject == GameObject.PlayerBot));
			//var botLeft = down.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Left, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Right) || y?.GameObject == GameObject.PlayerBot));
			//var botRight = down.SelectMany(x => Map.Grid.GetCurTurnTilesOnSide(x.Location, Direction.Right, 1).Where(y => (y?.GameObject == GameObject.AiBot && y?.GameObjectDirection == TurnDirection.Left) || y?.GameObject == GameObject.PlayerBot));

		}

		private KeyValuePair<Direction, Direction> ShootEnemy(Direction direction, StrategyResult result)
		{
			var player = Map.Grid.GetPlayer();
			if (player.TurnDirection.GetDirection() == direction)
			{
				result.FirstAction = player.Shoot();
				var b = new KeyValuePair<Direction, Direction>(result.FirstAction, result.SecondAction);
				Log.Append($"ShootEnemy result: [{b.Key}, {b.Value}]");
				return b;
			}

			result.FirstAction = direction;
			result.SecondAction = player.Shoot();
			var q = new KeyValuePair<Direction, Direction>(result.FirstAction, result.SecondAction);
			Log.Append($"ShootEnemy result: [{q.Key}, {q.Value}]");
			return q;
		}

		private void FindSameVerticeEnemies(IEnumerable<Tile> side, List<Tank> enemies, List<SameVerticeEnemy> sameVerticeEnemies)
		{
			var i = 0;
			foreach (var item in side)
			{
				i++;
				if (item?.GameObject == GameObject.AiBot || item?.GameObject == GameObject.PlayerBot)
				{
					var tank = enemies.FirstOrDefault(x => x.Location.Location.Equals(item.Location));
					if (tank != null)
						sameVerticeEnemies.Add(new SameVerticeEnemy
						{
							Tank = tank,
							TilesCount = i,
							DirectionFromPlayer = Direction.Left
						});
					break;
				}
			}
		}

		public void Init(int priority)
		{
			Priority = priority;
		}

		public void Update(Map map)
		{
			Map = map;
		}

		private class SameVerticeEnemy
		{
			public Tank Tank { get; set; }
			public int TilesCount { get; set; }
			public Direction DirectionFromPlayer { get; set; }
		}
	}
}

﻿using API.Misc;
using API.PathFinder.Models;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.CleverStrategies
{
	public class MoveToActionStrategy : ICleverStrategy
	{
		protected const int MaxDistToClaster = 80;
		protected Map Map;
		protected Tile SelectedDestination;

		public int Priority { get; private set; }

		public void Init(int priority)
		{
			Priority = priority;
		}

		public void Update(Map map)
		{
			Map = map;
		}

		public virtual StrategyResult Apply()
		{
			var result = new StrategyResult();
			var playerLoc = Map.PlayerTank.Location;

			var enemyClasters = DestinationSelector.DBSCAN(Map.Grid);
			var parameters = Map.Parameters;
			var minDistsToClaster = new List<Tuple<double, List<Tile>>>();
			foreach (var claster in enemyClasters)
			{
				var minDist = claster.Select(x => new { q = x, o = x.Location.Distance(playerLoc.Location) }).OrderBy(x => x.o);
				minDistsToClaster.Add(new Tuple<double, List<Tile>>(minDist.FirstOrDefault()?.o ?? double.MaxValue, claster));
			}

			var ordered = minDistsToClaster.OrderBy(x => x.Item2.Count).ThenBy(x => x.Item1);
			if (SelectedDestination != null)
			{
				//check if enemy destroyed
				var possibleNewLocations = SelectedDestination.Location.Neighbors();
				var possibleNewTiles = possibleNewLocations.Select(x => Map.Grid.GetTile(x.X, x.Y)).ToList();
				var curAim = possibleNewTiles.FirstOrDefault(x => x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot);
				if (curAim == null)
				{
					Log.Append($"Previous enemy destroyed.");
					SelectedDestination = null;
				}
				else//get new movement point to selected enemy
				{
					var bestAim = ordered.FirstOrDefault(x => x.Item1 < MaxDistToClaster) ?? ordered.FirstOrDefault();
					if (curAim.Location.Distance(playerLoc.Location) / 3 < bestAim.Item1)
					{
						SelectedDestination = curAim;
						Log.Append($"Hunting previous aim at {SelectedDestination.Location}");
					}
					else
					{
						SelectedDestination = bestAim.Item2.First();
						Log.Append($"Previous aim is too far {curAim.Location}, Starting hunt new aim {SelectedDestination.Location}");
					}
				}
			}

			if (SelectedDestination == null)
			{
				SelectedDestination = ordered.FirstOrDefault(x => x.Item1 < MaxDistToClaster)?.Item2.First() ?? ordered.FirstOrDefault().Item2.First();
			}			

			var route = PathFinder.Models.PathFinder.FindPath(Map.Grid, playerLoc, SelectedDestination);
			if (route.Count > 1)
			{
				result.FirstAction = Utilities.GetDirection(playerLoc.Location, route[1].Location);
				result.MovementDestination = route[1].Location;
			}

			return result;
		}
	}
}

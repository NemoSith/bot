﻿using API.Data;

namespace API.Strategies
{
	public static class ActionStrategyFactory
	{
		public static IActionStrategy GetStrategy(StrategyType strategy)
		{
			switch (strategy)
			{
				case StrategyType.Random:
					return new RandomActionStrategy();
				case StrategyType.Nearest:
					return new HuntNearest();
				case StrategyType.NearestClaster:
					return new MoveToNearestClaster();
				default:
					return new RandomActionStrategy();
			}
		}
	}
}

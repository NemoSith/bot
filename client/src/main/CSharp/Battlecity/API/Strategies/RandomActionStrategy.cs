﻿using Battlecity.API;
using System;

namespace API.Strategies
{
	public class RandomActionStrategy : IActionStrategy
	{
		private Random rand = new Random();

		public RandomActionStrategy()
		{ }

		public void Update(Board board)
		{
		}

		public string PerformAction()
		{
			var val = rand.Next((int)Direction.Left, (int)Direction.Down);
			return Direction.Act.ToString() + "," + ((Direction)val).ToString();
		}
	}
}

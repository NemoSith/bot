﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Misc;
using Battlecity.API;

namespace API.Strategies
{
	public class HuntNearest : IActionStrategy
	{
		private const string _escapeTileCodes = @"☼~╬╩╦╠╣╨╥╞╡│─┌┐└┘•";
		private const string _exceptionalWallBreakCodes = @"╬╩╦╠╣╨╥╞╡│─┌┐└┘";
		private Board _board;
		private bool[][] obstacleMap;
		private Point prevLocation;
		private int ticksToReload = 0;
		private List<Point> _bullets;
		private List<Point> _enemies;

		private const int MaxTooCloseDist = 3;

		public void Update(Board board)
		{
			_board = board;
			if(_board.IsGameOver())
			{
				ticksToReload = 0;
			}

			obstacleMap = _board.Field.Select(x => x.Select(y => _escapeTileCodes.Contains(y)).ToArray()).ToArray();
			ticksToReload--;
			_board.CanShoot = ticksToReload < 1;
			_bullets = board.GetBullets();
			_enemies = _board.GetEnemies();
		}

		public string PerformAction()
		{
			if (IsPointUnderFire(_bullets, _board.PlayerPosition) && _board.CanShoot)
			{
				var bullets = _bullets.Select(x => new { point = x, dist = x.Distance(_board.PlayerPosition) }).OrderBy(x => x.dist).ToList();
				if(bullets.Any())
				{
					ticksToReload = _board.Parameters.tankTicksPerShoot;
					return Utilities.GetDirection(bullets.FirstOrDefault().point, _board.PlayerPosition).GetDirection() + "," + Direction.Act.ToString();
				}

			}

			var shootingDirection = GetShootDirection(_enemies);
			string movementDirection = null;
			if (string.IsNullOrWhiteSpace(shootingDirection) || shootingDirection.Length < Direction.Act.ToString().Length + 1)
			{
				movementDirection = GetMovementDirection(_enemies);

				if(string.IsNullOrWhiteSpace(shootingDirection))
				{
					Log.Append($"Decision: {movementDirection}");
					return movementDirection;
				}
			}
			else
			{
				Log.Append($"Decision: {shootingDirection}");
				return shootingDirection;
			}

			var dir = string.Join(",", shootingDirection, movementDirection);
			Log.Append($"Decision: {dir}");
			return dir;
		}

		private static bool IsPointUnderFire(List<Point> bullets, Point point)
		{
			return bullets.Any(x => x.X == point.X && Math.Abs(x.Y - point.Y) < MaxTooCloseDist) || bullets.Any(x => x.Y == point.Y && Math.Abs(x.X - point.X) < MaxTooCloseDist);
		}

		private string GetMovementDirection(List<Point> enemies)
		{
			var mostCloseEnemy = enemies.OrderBy(x => x.Distance(_board.PlayerPosition)).FirstOrDefault();
			var moveToPoint = GetNextMovePoint(_board.PlayerPosition, mostCloseEnemy);

			return Utilities.GetDirection(_board.PlayerPosition, moveToPoint).GetDirection();
		}

		private string GetShootDirection(List<Point> enemies)
		{
			var horisontalEnemies = enemies.Where(x => x.X == _board.PlayerPosition.X).ToList();
			Log.Append($"There are horizontal enemies on: {string.Join(", ", horisontalEnemies)}");
			var shootDecisions = new Dictionary<Direction, List<KeyValuePair<Point, int>>>
			{
				{ Direction.Left, new List<KeyValuePair<Point, int>>() },
				{ Direction.Right, new List<KeyValuePair<Point, int>>() },
				{ Direction.Up, new List<KeyValuePair<Point, int>>() },
				{ Direction.Down, new List<KeyValuePair<Point, int>>() }
			};
			var anyHor = horisontalEnemies.Any();
			if (anyHor)
			{
				var obstaclesHorizontal = obstacleMap.GetVectorFromArray(_board.PlayerPosition.Y);
				foreach (var enemy in horisontalEnemies)
				{
					var yDif = enemy.Y - _board.PlayerPosition.Y;
					var dif = Math.Abs(yDif);
					if (dif < MaxTooCloseDist)
					{
						Log.Append($"Too close enemy at: {enemy}, distance: {dif}");
						var dir = Utilities.GetDirection(_board.PlayerPosition, enemy).GetDirection() + "," + Direction.Act;
						ticksToReload = _board.Parameters.tankTicksPerShoot;
						Log.Append($"Too close enemy, shooting: {dir}");
						return dir;
					}

					if (yDif < 0)
					{
						//Enemy is on left side
						if (obstaclesHorizontal.Skip(enemy.Y).Take(dif).All(x => !x))
						{
							Log.Append($"There is enemy to the left without obstacles: {enemy}, distance: {dif}");
							shootDecisions[Direction.Left].Add(new KeyValuePair<Point, int>(enemy, dif));
						}
					}
					else
					{
						//Enemy is on right side
						if (obstaclesHorizontal.Skip(_board.PlayerPosition.Y).Take(dif).All(x => !x))
						{
							Log.Append($"There is enemy to the right without obstacles: {enemy}, distance: {dif}");
							shootDecisions[Direction.Right].Add(new KeyValuePair<Point, int>(enemy, dif)); 
						}
					}
				}
			}

			var verticalEnemies = enemies.Where(x => x.Y == _board.PlayerPosition.Y).ToList();
			Log.Append($"There are vertical enemies on: {string.Join(", ", verticalEnemies)}");
			var anyVer = verticalEnemies.Any();
			if (anyVer)
			{
				var obstaclesVertical = obstacleMap.GetVectorFromArray(_board.PlayerPosition.X, false);
				foreach (var enemy in verticalEnemies)
				{
					var xDif = enemy.X - _board.PlayerPosition.X;
					var dif = Math.Abs(xDif);
					if (dif < MaxTooCloseDist)
					{
						Log.Append($"Too close enemy at: {enemy}, distance: {dif}");
						var dir = Utilities.GetDirection(_board.PlayerPosition, enemy).GetDirection() + "," + Direction.Act;
						ticksToReload = _board.Parameters.tankTicksPerShoot;
						Log.Append($"Too close enemy, shooting: {dir}");
						return dir;
					}

					if (xDif < 0)
					{
						//Enemy is on bottom side
						if (obstaclesVertical.Skip(enemy.X).Take(dif).All(x => !x))
						{
							Log.Append($"There is enemy to the bot without obstacles: {enemy}, distance: {dif}");
							shootDecisions[Direction.Down].Add(new KeyValuePair<Point, int>(enemy, dif));
						}
					}
					else
					{
						//Enemy is on top side
						if (obstaclesVertical.Skip(_board.PlayerPosition.X).Take(dif).All(x => !x))
						{
							Log.Append($"There is enemy to the top without obstacles: {enemy}, distance: {dif}");
							shootDecisions[Direction.Up].Add(new KeyValuePair<Point, int>(enemy, dif));
						}
					}
				}
			}

			if((!anyHor && !anyVer) || shootDecisions.All(x => !x.Value.Any()))
			{
				return null;
			}

			var shootDirection = shootDecisions.OrderByDescending(x => x.Value.Count).FirstOrDefault().Key;
			Log.Append($"There are alot of enemies to the {shootDirection}");
			var result = shootDirection == _board.PlayerOrientation ? Direction.Act.ToString() : shootDirection.ToString() + "," + Direction.Act.ToString();
			ticksToReload = _board.Parameters.tankTicksPerShoot;
			Log.Append($"GetShootDirection says do: {result}");
			return result;
		}

		private Point GetNextMovePoint(Point start, Point end)
		{
			try
			{
				Log.Append($"Want to move from {start} to {end}");
				var neighbors = start.Neighbors();
				if (neighbors.Any(x => x.Equals(end)))
				{
					if(IsPointUnderFire(_bullets, end))
					{
						Log.Append($"End point is under atack, will stay at this place {start}");
						return start;
					}

					Log.Append($"Moving directly to end point {end}");
					return end;
				}

				var notObst = neighbors
					.Where(x => !obstacleMap[x.X][x.Y] && !x.Equals(prevLocation))
					.Select(x => new { point = x, dist = x.Distance(end) })
					.OrderBy(x => x.dist)
					.ToList();
				var underAtack = notObst.Where(x => IsPointUnderFire(_bullets, x.point)).Select(x => x.point).ToList();
				Log.Append($"There are some next points under atack: {string.Join(", ", underAtack)}");
				notObst = notObst.Where(x => !underAtack.Any(y => y.Equals(x.point))).ToList();
				Log.Append($"Priority of next points: {string.Join(", ", notObst.Select(x => $"[{x.point}] == {x.dist}"))}");
				if(notObst.Any())
				{
					Log.Append($"Next point is: {notObst.FirstOrDefault().point}");
					return notObst.FirstOrDefault().point;
				}
				else
				{
					if(IsPointUnderFire(_bullets, prevLocation))
					{
						Log.Append($"Prev point is under atack will stay here: {start}");
						return start;
					}

					Log.Append($"Moving to prev point: {prevLocation}");
					return prevLocation;
				}
			}
			finally
			{
				prevLocation = start;
			}
		}
	}
}

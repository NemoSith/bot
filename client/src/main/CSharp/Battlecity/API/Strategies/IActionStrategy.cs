﻿using Battlecity.API;

namespace API.Strategies
{
	public interface IActionStrategy
	{
		void Update(Board board);
		string PerformAction();
	}
}

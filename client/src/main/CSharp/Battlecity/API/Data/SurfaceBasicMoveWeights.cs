﻿namespace API.Data
{
	public class SurfaceBasicMoveWeights
	{
		public int Ground { get; set; }
		public int Water { get; set; }
		public int Wall { get; set; }
		public int WallBroken1 { get; set; }
		public int WallBroken2 { get; set; }
		public int StrongWall { get; set; }
		public int Ice { get; set; }
		public int Trees { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
	public enum StrategyType
	{
		Random = 0,
		Nearest = 1,
		NearestClaster = 2,
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
	[Serializable]
	public class Parameters
	{
		public int aiPrizeLimit { get; set; }
		public int penaltyWalkingOnWater { get; set; }//Кількість штрафних тіків-затримки на воді
		public bool showMyTankUnderTree { get; set; }//Бачите ви свій танк поверх дерев, чи ні
		public int killYourTankPenalty { get; set; }//Штрафні бали, коли гине ваш танк
		public int killOtherHeroTankScore { get; set; }//Бали, які ви заробляєтете, знищуючи інших ботів
		public int killOtherAiTankScore { get; set; }//Бали за знищення AI-ботів
		public int spawnAiPrize { get; set; }
		public int killHitsAiPrize { get; set; }//Кількість пострілів, які потрібно зробити по призовому танку
		public int prizeOnField { get; set; }//Час існування призу на полі
		public int prizeWorking { get; set; }//Час впливу призу на танк
		public int aiTicksPerShoot { get; set; }//Затримка між пострілами для AI-танків
		public int tankTicksPerShoot { get; set; }//Затримка між пострілами звичайних танків
		public int slipperiness { get; set; }//Кількість тіків, які танк ковзає по кризі
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Misc
{
	public static class Log
	{
		private static readonly StringBuilder LogBuilder = new StringBuilder();

		public static void Append(string text)
		{
			LogBuilder.AppendLine($"{DateTime.Now} ---> {text}");
		}

		public static void AppendExact(string text)
		{
			LogBuilder.AppendLine(text);
		}

		public new static string ToString()
		{
			return LogBuilder.ToString();
		}
	}
}

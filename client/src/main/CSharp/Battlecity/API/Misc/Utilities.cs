﻿/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2020 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

using API.Misc;
using API.PathFinder.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Battlecity.API
{
    public static class Utilities
    {
		public static Direction OpositDirection(this Direction dir)
		{
			if (dir == Direction.Down) return Direction.Up;
			if (dir == Direction.Up) return Direction.Down;
			if (dir == Direction.Left) return Direction.Right;
			if (dir == Direction.Right) return Direction.Left;
			return Direction.None;
		}

		public static TurnDirection GetTurnDirection(this Direction dir)
		{
			return (TurnDirection)(int)dir;
		}

		public static Direction GetDirection(this TurnDirection dir)
		{
			return (Direction)(int)dir;
		}

		public static Direction GetDirection1(this Point start, Point end)
		{
			if (end == null)
			{
				return Direction.None;
			}

			if (start.X.Equals(end.X))
			{
				if (start.Y.Equals(end.Y))
				{
					return Direction.None;
				}

				return start.Y > end.Y ? Direction.Down : Direction.Up;
			}

			return start.X > end.X ? Direction.Left : Direction.Right;
		}

		public static Direction GetDirection(Point start, Point end)
	    {
			if(end == null)
			{
				return Direction.None;
			}

		    if (start.X.Equals(end.X))
		    {
			    if (start.Y.Equals(end.Y))
				{
					return Direction.None;
				}

			    return start.Y > end.Y ? Direction.Down : Direction.Up;
			}

		    return start.X > end.X ? Direction.Left : Direction.Right;
}

	    public static string GetDirection(this Direction dir)
	    {
		    return dir == Direction.None ? string.Empty : dir.ToString();
		}

		public static string GetDirection(this KeyValuePair<Direction, Direction> dir)
		{
			var frst = dir.Key.GetDirection();
			var scnd = dir.Value.GetDirection();
			if (!string.IsNullOrWhiteSpace(frst))
			{
				if (string.IsNullOrWhiteSpace(scnd)) return frst;
				return frst + "," + scnd;
			}

			if(!string.IsNullOrWhiteSpace(scnd))
			{
				if (string.IsNullOrWhiteSpace(frst)) return scnd;
				return frst + "," + scnd;
			}

			return string.Empty;
		}

		public static bool[] GetVectorFromArray(this bool[][]array, int index, bool horizontal = true)
	    {
		    if (horizontal)
			    return array[index];

		    var result = new bool[array.Length];
		    for (int i = 0; i < result.Length; i++)
		    {
			    result[i] = array[i][index];
		    }

		    return result;
	    }

	    public static IEnumerable<string> Split(string str, int chunkSize)
	    {
		    return Enumerable.Range(0, str.Length / chunkSize)
			    .Select(i => str.Substring(i * chunkSize, chunkSize));
	    }

		public static void MeasureOperationTime(Action action, string name)
	    {
		    var timer = new Stopwatch();
		    timer.Start();
		    action();
		    timer.Stop();
		    Log.Append($"Operation '{name}' performed in {timer.ElapsedMilliseconds}ms");
	    }

	    public static T MeasureOperationTime<T>(Func<T> action, string name)
	    {
		    var timer = new Stopwatch();
		    timer.Start();
		    var t = action();
		    timer.Stop();
		    Log.Append($"Operation '{name}' performed in {timer.ElapsedMilliseconds}ms");
		    return t;
	    }

        public static string GetDescription(this Element value)
        {
            var type = value.GetType();
            string name = Enum.GetName(type, value);

            if (name != null)
            {
                var field = type.GetField(name);

                var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                    return attribute.Description;
            }

            return null;
        }

        public static Element GetElement(this string value)
        {
            var type = typeof(Element);

            foreach (string name in Enum.GetNames(type))
            {
                var field = type.GetField(name);

                var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute == null || attribute.Description != value)
                    continue;

                return (Element)field.GetValue(field.Name);
            }

            throw new Exception("There is no such an Element constant!");
        }
    }
}

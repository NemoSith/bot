﻿namespace API.PathFinder.Models
{
	public class Bullet
	{
		public const char Char = '•';
		public Tile Location { get; set; }
		public TurnDirection Direction { get; set; }
		public bool IsConfident { get; set; }

		public override string ToString()
		{
			return $"Bullet on {Location.Location} Looking at {Direction}";
		}
	}
}

﻿using System.Collections.Generic;
using System.Linq;

namespace API.PathFinder.Models
{
	public static class DestinationSelector
	{
		public const int maxDist = 50;

		public static List<List<Tile>> DBSCAN(Grid grid)
		{
			var enemies = grid.GetEnemies().Select(x => new ClasterNode
			{
				Tile = x.Location,
				Flag = Flag.Red,
				Neighbors = new List<Tile>()
			});
			var points = new List<ClasterNode>();
			foreach (var enemy in enemies)
			{
				var thisNode = new ClasterNode
				{
					Tile = enemy.Tile,
					Flag = Flag.Red,
					Neighbors = new List<Tile>()
				};
				points.Add(thisNode);
				foreach (var otherEnemy in enemies)
				{
					if (enemy == otherEnemy) continue;
					var dist = GetDist(enemy.Tile, otherEnemy.Tile);
					if (dist > maxDist) continue;
					thisNode.Neighbors.Add(otherEnemy.Tile);
				}

				if (thisNode.Neighbors.Count > 3)
				{
					enemy.Flag = Flag.Green;
					thisNode.Flag = Flag.Green;
				}
			}

			var clasters = new List<KeyValuePair<int, List<ClasterNode>>>();
			var currentClasterId = 1;
			var alreadyAdded = new List<ClasterNode>();
			foreach(var point in points)
			{
				if(alreadyAdded.Any(x => x == point)) continue;
				alreadyAdded.Add(point);
				var lst = new List<ClasterNode>();
				lst.Add(point);
				var stack = new Stack<ClasterNode>();
				stack.Push(point);
				do
				{
					var cur = stack.Pop();
					foreach (var neighbor in cur.Neighbors)
					{
						var neigborPoint = points.FirstOrDefault(x => x.Tile.Location.Equals(neighbor.Location));
						if (cur.Flag == Flag.Red && neigborPoint.Flag == Flag.Green) cur.Flag = Flag.Yellow;
						if (alreadyAdded.Any(x => x == neigborPoint)) continue;						
						alreadyAdded.Add(neigborPoint);
						stack.Push(neigborPoint);
						lst.Add(neigborPoint);
					}
				} while (stack.Any());

				clasters.Add(new KeyValuePair<int, List<ClasterNode>>(currentClasterId, lst));
			}

			return clasters.Select(x => x.Value.Select(y => y.Tile).ToList()).ToList();
		}

		public static Tile GetNearestAiBot(Grid grid)
		{
			var player = grid.GetPlayer();
			if(player == null)
			{
				return null;
			}

			return grid.GetAiBots().OrderBy(x => x.Location.Location.Distance(player.Location.Location)).FirstOrDefault()?.Location;
		}

		private static double GetDist(Tile one, Tile two)
		{
			return two.Location.Distance(one.Location);
		}

		private class ClasterNode
		{
			public Tile Tile { get; set; }
			public Flag Flag { get; set; }
			public int ClasterId { get; set; }
			public List<Tile> Neighbors { get; set; }
		}

		private enum Flag
		{
			Red,
			Yellow,
			Green
		}
	}
}

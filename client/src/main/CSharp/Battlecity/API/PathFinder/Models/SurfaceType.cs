﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public enum SurfaceType
	{
		Unknown,//Overlaped by something else
		Ground,
		Water,
		Wall,
		WallBroken1,
		WallBroken2,
		StrongWall,
		Ice,
		Trees
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public enum TurnDirection
	{
		Unknown = -1,
		None = 0,
		Left = 1,
		Right = 2,
		Up = 3,
		Down = 4
	}
}

﻿using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public static class PathFinder
	{
		public static List<Tile> FindPath(Grid grid, Tile start, Tile target)
		{
			if (start == null)
				return new List<Tile>();

			// Шаг 1.
			var closedSet = new List<Node>();
			var openSet = new List<Node>();
			// Шаг 2.
			Node startNode = new Node()
			{
				PrevNode = null,
				CurPoint = start,
				TotalMoveCost = 0,
				Distance = target.Location.Distance(start.Location),
				MoveCost = 0
			};
			openSet.Add(startNode);
			while (openSet.Count > 0)
			{
				// Шаг 3.
				var currentNode = openSet
					.OrderBy(node => node.MoveCost)
					.First();
				// Шаг 4.
				if (currentNode.CurPoint.Location.Equals(target.Location))
				{
					return GetPathForNode(currentNode); 
				}

				// Шаг 5.
				openSet.Remove(currentNode);
				closedSet.Add(currentNode);
				// Шаг 6.
				var neighbors = GetNeighborsSorted(grid, currentNode, target);

				foreach (var neighbourNode in neighbors)
				{
					// Шаг 7.
					if (closedSet.Any(node => node.CurPoint.Location.Equals(neighbourNode.CurPoint.Location)))
						continue;
					var openNode = openSet.FirstOrDefault(node => node.CurPoint.Location.Equals(neighbourNode.CurPoint.Location));
					// Шаг 8.
					if (openNode == null)
						openSet.Add(neighbourNode);
					else
					{
						if (openNode.TotalMoveCost > neighbourNode.TotalMoveCost)
						{
							// Шаг 9.
							openNode.PrevNode = currentNode;
							openNode.TotalMoveCost = neighbourNode.TotalMoveCost;
						}
					}
				}
			}

			// Шаг 10.
			return new List<Tile>();
		}

		private static List<Tile> GetPathForNode(Node pathNode)
		{
			var result = new List<Tile>();
			var currentNode = pathNode;
			while (currentNode != null)
			{
				result.Add(currentNode.CurPoint);
				currentNode = currentNode.PrevNode;
			}

			result.Reverse();
			return result;
		}

		private static List<Node> GetNeighborsSorted(Grid grid, Node current, Tile target)
		{
			return grid
				.GetNeighbors(current.CurPoint)
				.Where(x => !x.IsObstacle)
				.Select(x => new Node
				{
					PrevNode = current,
					CurPoint = x,
					Distance = x.Location.Distance(target.Location),
					MoveCost = x.Location.Distance(target.Location) 
						+ x.CrossWeight //InitialCost equal to distance and cost of crossing this tile
						+ (grid.CheckNextTurnBulletHit(x.Location) ? BulletHitCost : 0) //Do not step into bullet
						+ (x.Location.IsSameVertice(target.Location) ? -500 : 10),//Come on same vertice as target
					TotalMoveCost = current.TotalMoveCost + x.Location.Distance(target.Location) + x.CrossWeight
				})
				.OrderBy(x => x.MoveCost)
				.ToList();
		}

		public static List<Tile> FindPathThroughWalls(Grid grid, Tile start, Tile target)
		{
			if (start == null)
				return new List<Tile>();

			// Шаг 1.
			var closedSet = new List<Node>();
			var openSet = new List<Node>();
			// Шаг 2.
			Node startNode = new Node()
			{
				PrevNode = null,
				CurPoint = start,
				TotalMoveCost = 0,
				Distance = target.Location.Distance(start.Location),
				MoveCost = 0
			};
			openSet.Add(startNode);
			while (openSet.Count > 0)
			{
				// Шаг 3.
				var currentNode = openSet
					.OrderBy(node => node.MoveCost)
					.First();
				// Шаг 4.
				if (currentNode.CurPoint.Location.Equals(target.Location))
				{
					return GetPathForNode(currentNode);
				}

				// Шаг 5.
				openSet.Remove(currentNode);
				closedSet.Add(currentNode);
				// Шаг 6.
				var neighbors = GetNeighborsSorted(grid, currentNode, target);

				foreach (var neighbourNode in neighbors)
				{
					// Шаг 7.
					if (closedSet.Any(node => node.CurPoint.Location.Equals(neighbourNode.CurPoint.Location)))
						continue;
					var openNode = openSet.FirstOrDefault(node => node.CurPoint.Location.Equals(neighbourNode.CurPoint.Location));
					// Шаг 8.
					if (openNode == null)
						openSet.Add(neighbourNode);
					else
					{
						if (openNode.TotalMoveCost > neighbourNode.TotalMoveCost)
						{
							// Шаг 9.
							openNode.PrevNode = currentNode;
							openNode.TotalMoveCost = neighbourNode.TotalMoveCost;
						}
					}
				}
			}

			// Шаг 10.
			return new List<Tile>();
		}

		private static List<Node> GetWithWallsNeighborsSorted(Grid grid, Node current, Tile target)
		{
			return grid
				.GetNeighbors(current.CurPoint)
				.Select(x => new Node
				{
					PrevNode = current,
					CurPoint = x,
					Distance = x.Location.Distance(target.Location),
					MoveCost = x.Location.Distance(target.Location)
						+ x.CrossWeight //InitialCost equal to distance and cost of crossing this tile
						+ (grid.CheckNextTurnBulletHit(x.Location) ? BulletHitCost : 0) //Do not step into bullet
						+ (x.Location.IsSameVertice(target.Location) ? -200 : 10),//Come on same vertice as target
					TotalMoveCost = current.TotalMoveCost + x.Location.Distance(target.Location) + x.CrossWeight
				})
				.OrderBy(x => x.MoveCost)
				.ToList();
		}

		private class Node
		{
			public Node PrevNode { get; set; }
			public Tile CurPoint { get; set; }
			public double Distance { get; set; }
			public double MoveCost { get; set; }
			public double TotalMoveCost { get; set; }
		}

		private const int BulletHitCost = 5000000;
	}
}

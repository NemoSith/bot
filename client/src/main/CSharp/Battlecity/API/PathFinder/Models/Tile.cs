﻿using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public class Tile
	{
		private const string BrokenWalls1 = "╩╦╠╣";
		private const string BrokenWalls2 = "╨╥╞╡│─┌┐└┘";
		private const char WallChar = '╬';
		private const char GroundChar = ' ';
		private const char StrongWallChar = '☼';
		private const char IceChar = '#';
		private const char WaterChar = '~';
		private const char TreesChar = '%';

		private const char BangChar = 'Ѡ';
		private const char BulletChar = '•';
		public const string PlayerTank = "▲►▼◄";
		private const string PlayerBotTank = "˄˃˅˂";
		private const string AiBotTank = "?»¿«◘";
		private const string Bonus = "!12345";

		public SurfaceType Surface { get; set; }
		public GameObject GameObject { get; set; }
		public TurnDirection GameObjectDirection { get; set; }
		public Point Location { get; set; }
		public bool IsObstacle { get; set; }
		public bool IsBulletObstacle { get; set; }
		public int CrossWeight { get; set; }
		public char OriginalChar { get; set; }
		
		public Tile(Point location, char surface)
		{
			IsObstacle = false;
			IsBulletObstacle = false;
			Location = location;
			OriginalChar = surface;
			Surface = GetSurfaceType();
			GameObject = GetGameObject();
			CrossWeight = GetWeight();
		}

		public Tile()
		{
		}

		public void Update(char surface)
		{
			IsObstacle = false;
			IsBulletObstacle = false;
			OriginalChar = surface;
			Surface = GetSurfaceType();
			GameObject = GetGameObject();
			CrossWeight = GetWeight();
		}

		public Tile Copy()
		{
			return new Tile
			{
				CrossWeight = this.CrossWeight,
				IsObstacle = this.IsObstacle,
				Location = this.Location,
				OriginalChar = this.OriginalChar,
				Surface = this.Surface,
				GameObject = this.GameObject,
				GameObjectDirection = this.GameObjectDirection
			};
		}

		public Tile CopySurface()
		{
			return new Tile
			{
				CrossWeight = this.CrossWeight,
				IsObstacle = this.IsObstacle,
				Location = this.Location,
				OriginalChar = GetOriginalCharBySurface(),
				Surface = this.Surface
			};
		}

		private char GetOriginalCharBySurface()
		{
			if (GameObject == GameObject.None) return OriginalChar;
			else return GroundChar;
		}

		private SurfaceType GetSurfaceType()
		{
			if(OriginalChar == StrongWallChar)
			{
				IsObstacle = true;
				IsBulletObstacle = true;
				return SurfaceType.StrongWall;
			}

			if(OriginalChar == GroundChar)
			{
				return SurfaceType.Ground;
			}

			if(OriginalChar == WallChar)
			{
				IsObstacle = true;
				IsBulletObstacle = true;
				return SurfaceType.Wall;
			}

			if(BrokenWalls1.Contains(OriginalChar))
			{
				IsObstacle = true;
				IsBulletObstacle = true;
				return SurfaceType.WallBroken1;
			}

			if (BrokenWalls2.Contains(OriginalChar))
			{
				IsObstacle = true;
				IsBulletObstacle = true;
				return SurfaceType.WallBroken2;
			}

			if (OriginalChar == TreesChar)
			{
				return SurfaceType.Trees;
			}

			if (OriginalChar == IceChar)
			{
				return SurfaceType.Ice;
			}

			if (OriginalChar == WaterChar)
			{
				IsObstacle = true;
				return SurfaceType.Water;
			}

			return Surface;
		}

		private int GetWeight()
		{
			if (OriginalChar == StrongWallChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.StrongWall;
			}

			if (OriginalChar == GroundChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.Ground;
			}

			if (OriginalChar == WallChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.Wall;
			}

			if (BrokenWalls1.Contains(OriginalChar))
			{
				return PredefinedData.SurfaceBasicMoveWeights.WallBroken1;
			}

			if (BrokenWalls2.Contains(OriginalChar))
			{
				return PredefinedData.SurfaceBasicMoveWeights.WallBroken2;
			}

			if (OriginalChar == TreesChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.Trees;
			}

			if (OriginalChar == IceChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.Ice;
			}

			if (OriginalChar == WaterChar)
			{
				return PredefinedData.SurfaceBasicMoveWeights.Water;
			}

			return PredefinedData.SurfaceBasicMoveWeights.Ground;
		}

		private GameObject GetGameObject()
		{
			GameObjectDirection = TurnDirection.None;
			if (OriginalChar == BangChar)
			{
				return GameObject.Bang;
			}

			if (OriginalChar == BulletChar)
			{
				return GameObject.Bullet;
			}

			if (Bonus.Contains(OriginalChar))
			{
				return GameObject.Bonus;
			}

			var i = PlayerTank.IndexOf(OriginalChar);
			if (i > -1)
			{
				if (i == 0) GameObjectDirection = TurnDirection.Up;
				if (i == 1) GameObjectDirection = TurnDirection.Right;
				if (i == 2) GameObjectDirection = TurnDirection.Down;
				if (i == 3) GameObjectDirection = TurnDirection.Left;
				return GameObject.PlayerTank;
			}

			i = PlayerBotTank.IndexOf(OriginalChar);
			if (PlayerBotTank.Contains(OriginalChar))
			{
				if (i == 0) GameObjectDirection = TurnDirection.Up;
				if (i == 1) GameObjectDirection = TurnDirection.Right;
				if (i == 2) GameObjectDirection = TurnDirection.Down;
				if (i == 3) GameObjectDirection = TurnDirection.Left;
				return GameObject.PlayerBot;
			}

			i = AiBotTank.IndexOf(OriginalChar);
			if (AiBotTank.Contains(OriginalChar))
			{
				if (i == 0) GameObjectDirection = TurnDirection.Up;
				if (i == 1) GameObjectDirection = TurnDirection.Right;
				if (i == 2) GameObjectDirection = TurnDirection.Down;
				if (i == 3) GameObjectDirection = TurnDirection.Left;
				if (i == 4) GameObjectDirection = TurnDirection.Unknown;
				return GameObject.AiBot;
			}

			return GameObject.None;
		}
	}
}

﻿namespace API.PathFinder.Models
{
	public enum GameObject
	{
		None,
		PlayerBot,
		AiBot,
		PlayerTank,
		Bullet,
		Bonus,
		Bang
	}
}

﻿using API.Components;
using API.Misc;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public class Tank
	{
		private const int ReloadTime = 4;

		public TankType Type { get; set; }
		public bool CanShoot => TurnsToReload < 1;
		public Tile Location { get; set; }
		public TurnDirection TurnDirection { get; set; }
		public int TurnsToReload { get; set;}

		public Tank(Tile tile)
		{
			Location = tile;
			TurnDirection = tile.GameObjectDirection;
			Type = (TankType)(int)tile.GameObject;
		}

		public Tank(TankType player)
		{
			Location = new Tile();
			Location.Location = new Point(0,0);
			Type = player;
		}

		public void Update()
		{
			TurnDirection = Location.GameObjectDirection.GetDirection().GetTurnDirection();
			TurnsToReload--;
		}

		public Direction Move(Point moveTo)
		{
			return Utilities.GetDirection(Location.Location, moveTo);
		}

		public Direction Shoot()
		{
			var result = CanShoot ? Battlecity.API.Direction.Act : Battlecity.API.Direction.None;
			TurnsToReload = ReloadTime;
			Log.Append($"Player tank shoots with result {result}, reloading in {TurnsToReload}");
			return result;
		}

		public KeyValuePair<Direction, Direction> Shoot(Point shootTo)
		{
			var moveDir = Move(shootTo);
			if((int)moveDir == ((int)TurnDirection) + 1)
			{
				return new KeyValuePair<Direction, Direction>(Shoot(), Direction.None);
			}

			return new KeyValuePair<Direction, Direction>(moveDir, Shoot());
		}

		public KeyValuePair<Direction, Direction> ShootAndMove(Point moveTo)
		{
			return new KeyValuePair<Direction, Direction>(Shoot(), Move(moveTo));
		}

		public KeyValuePair<Direction, Direction> ShootAndMove(Point shootTo, Point moveTo, bool shootingPriority)
		{
			var shooting = Shoot(shootTo);
			var shoot = shooting.Value;
			var shootTurn = shooting.Key;
			if(shootTurn == Direction.None)
			{
				return new KeyValuePair<Direction, Direction>(shoot, Move(moveTo));
			}

			return shootingPriority 
				? shooting
				: new KeyValuePair<Direction, Direction>(shoot, Move(moveTo));
		}

		public override string ToString()
		{
			var shooter = string.Empty;
			if(Type == TankType.Player)
				shooter = CanShoot ? " and can shoot" : $" reloading in {TurnsToReload} turns";
			return $"{Type} Tank on {Location.Location} Looking at {TurnDirection}{shooter}";
		}
	}
}

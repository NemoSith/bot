﻿using API.Data;
using API.Misc;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.PathFinder.Models
{
	public class Map
	{
		private int MinPlayerEnemiesToPlay = 7;

		protected string BoardString { get; private set; }
		public LengthToXY LengthXy { get; private set; }
		public string[] Field { get; private set; }
		public Tank PlayerTank { get; private set; }
		public Parameters Parameters { get; private set; }
		public int RoundNum { get; private set; }
		public Grid Grid { get; private set; }
		public bool IsGameGoOn { get; private set; }

		public void SetPlayerTankPosition(Point loc)
		{
			var tile = Grid.GetTile(loc.X, loc.Y);
			tile.GameObject = GameObject.PlayerTank;
			tile.GameObjectDirection = TurnDirection.Up;
			tile.OriginalChar = '▲';
			PlayerTank = new Tank(tile);
		}

		public void SetPathTile(Point loc)
		{
			var tile = Grid.GetTile(loc.X, loc.Y);
			tile.OriginalChar = 'X';
		}

		public Map(Parameters parameters)
		{
			Parameters = parameters;
			RoundNum = 1;
			IsGameGoOn = false;
		}

		public void Update(string boardString)
		{
			BoardString = boardString.Replace("\n", "");
			LengthXy = new LengthToXY(Size);
			if (Grid == null) Grid = new Grid(this);
			if (IsGameOver())
			{
				Log.AppendExact($"===Round { RoundNum } finished!===\n");
				IsGameGoOn = false;
				RoundNum++;
				Grid.Wipe(Size);
			}
			else
			{
				if (!IsGameGoOn && Grid.GetPlayerBots().Count() > MinPlayerEnemiesToPlay)
				{
					Log.AppendExact($"===New Round { RoundNum } Started!===\n");
					IsGameGoOn = true;
				}
			}

			Grid.Update(boardString);
			PlayerTank = Grid.GetPlayer();
			Field = Utilities.Split(boardString, Size).ToArray();
		}

		public int Size => (int)Math.Sqrt(BoardString.Length);

		#region Base matrix

		public List<Point> Get(Element element)
		{
			List<Point> result = new List<Point>();

			for (int i = 0; i < Size * Size; i++)
			{
				Point point = LengthXy.GetXY(i);

				if (IsAt(point, element))
					result.Add(point);
			}

			return result;
		}

		public Element GetAt(Point point) => point.IsOutOf(Size)
			? Element.WALL
			: BoardString[LengthXy.GetLength(point.X, point.Y)].ToString().GetElement();

		public bool IsAt(Point point, Element element) => point.IsOutOf(Size)
			? false
			: GetAt(point).GetDescription() == element.GetDescription();

		public bool IsAnyOfAt(Point point, params Element[] elements) =>
			elements.Any(element => IsAt(point, element));

		public bool IsNear(Point point, Element element) => point.IsOutOf(Size)
			? false
			: IsAt(point.ShiftLeft(), element) || IsAt(point.ShiftRight(), element)
											   || IsAt(point.ShiftTop(), element) || IsAt(point.ShiftBottom(), element);

		public int CountNear(Point point, Element element)
		{
			if (point.IsOutOf(Size))
				return 0;

			int count = 0;

			if (IsAt(point.ShiftLeft(), element))
				count++;

			if (IsAt(point.ShiftRight(), element))
				count++;

			if (IsAt(point.ShiftTop(), element))
				count++;

			if (IsAt(point.ShiftBottom(), element))
				count++;

			return count;
		}

		#endregion

		public bool IsGameOver() => BoardString.All(x => !Tile.PlayerTank.Contains(x));

		public string BoardAsString()
		{
			string result = "";

			for (int i = 0; i < Size; i++)
			{
				result += BoardString.Substring(i * Size, Size);
				result += '\n';
			}

			return result;
		}

		public new string ToString()
		{
			string ListToString(List<Point> list) => string.Join(",", list.ToArray());
			var roundStage = IsGameOver() ? "round finished" : IsGameGoOn ? "playing" : "waiting players";
			return $"Round: {RoundNum} ({roundStage})\n\n" +
				   $"{Grid.GridToString()} \n" +
				   $"{PlayerTank} \n" +
				   $"Can shoot: {PlayerTank.CanShoot} \n" +
				   //$"Enemies at: \n{string.Join("\n", Grid.GetEnemies())} \n" +
				   $"Bullets at: \n{string.Join("\n", Grid.GetBullets())} \n";
		}
	}
}

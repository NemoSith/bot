﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public enum TankType
	{
		Bot = 1,
		Ai = 2,
		Player = 3
	}
}

﻿using API.Misc;
using Battlecity.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API.PathFinder.Models
{
	public class Grid
	{
		public Tile[,] PrevTurnMap { get; private set; }
		public Tile[,] CurrTurnMap { get; private set; }
		public Tile[,] PredictMap { get; private set; }
		public bool HasChanges { get; set; }

		private int Size;
		private LengthToXY LengthToXY;
		private Tank Player;
		private List<Tank> Tanks;
		private List<Tank> AiBots;
		private List<Tank> PlayerBots;
		private List<Bullet> Bullets;
		
		private List<Tank> PrevTurnTanks;
		private List<Tank> PrevTurnAiBots;
		private List<Tank> PrevTurnPlayerBots;
		private List<Bullet> PrevTurnBullets;

		public void SetPlayer(Tank tank)
		{
			Player = tank;
		}

		public Grid(Map map)
		{
			Wipe(map.Size);
		}

		public void Wipe(int size)
		{
			HasChanges = true;
			Size = size;
			LengthToXY = new LengthToXY(Size);
			PrevTurnMap = new Tile[Size, Size];
			CurrTurnMap = new Tile[Size, Size];
			PredictMap = new Tile[Size, Size];
			Tanks = new List<Tank>();
			AiBots = new List<Tank>();
			PlayerBots = new List<Tank>();
			Bullets = new List<Bullet>();
			Player = new Tank(TankType.Player);

			PrevTurnTanks = new List<Tank>();
			PrevTurnAiBots = new List<Tank>();
			PrevTurnPlayerBots = new List<Tank>();
			PrevTurnBullets = new List<Bullet>();
			for (int i = 0; i < Size; i++)
			{
				for (int j = 0; j < Size; j++)
				{
					var point = new Point(j, i);
					var curChar = ' ';
					PrevTurnMap[i, j] = new Tile(point, curChar);
					CurrTurnMap[i, j] = new Tile(point, curChar);
					PredictMap[i, j] = new Tile(point, curChar);
				}
			}
		}

		public void Update(string updatedStringMap)
		{
			PrevTurnTanks = Tanks.ToList();
			PrevTurnAiBots = AiBots.ToList();
			PrevTurnPlayerBots = PlayerBots.ToList();
			PrevTurnBullets = Bullets.ToList();
			PredictMap = new Tile[Size, Size];
			Tanks = new List<Tank>();
			AiBots = new List<Tank>();
			PlayerBots = new List<Tank>();
			Bullets = new List<Bullet>();
			
			HasChanges = false;
			for (int i = 0; i < Size; i++)
			{
				for (int j = 0; j < Size; j++)
				{
					var tile = CurrTurnMap[i, j];
					var prevTurnTile = PrevTurnMap[i, j];
					if (tile.GameObject != prevTurnTile.GameObject)
					{
						HasChanges = true;
					}

					PrevTurnMap[i, j] = tile.Copy();
					var coord = LengthToXY.GetLength(j, i);
					tile.Update(updatedStringMap[coord]);
					if(tile.GameObject == GameObject.AiBot)
					{
						var tank = new Tank(CurrTurnMap[i, j]);
						AiBots.Add(tank);
						Tanks.Add(tank);
					}

					if (tile.GameObject == GameObject.PlayerBot)
					{
						var tank = new Tank(CurrTurnMap[i, j]);
						PlayerBots.Add(tank);
						Tanks.Add(tank);
					}

					if (tile.GameObject == GameObject.PlayerTank)
					{
						Player.Location = CurrTurnMap[i, j];
						Player.Update();
						Log.Append(Player.ToString());
						Tanks.Add(Player);
					}

					if (tile.GameObject == GameObject.Bullet)
					{
						var bullet = new Bullet 
						{
							Location = tile,
							Direction = TurnDirection.Unknown,
							IsConfident = false
						};
						Bullets.Add(bullet);
					}

					PredictMap[i,j] = tile.CopySurface();
				}
			}

			Predict();
		}

		private void Predict()
		{
			foreach (var bullet in Bullets)
			{
				var left = GetPrevTurnTilesOnSide(bullet.Location.Location, Direction.Left, 2)
					.TakeWhile(x => x?.IsBulletObstacle == false)
					.Where(x => (x.GameObject == GameObject.Bullet && (x.GameObjectDirection == TurnDirection.Right || x.GameObjectDirection == TurnDirection.None)) || ((x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot || x.GameObject == GameObject.PlayerTank) && x.GameObjectDirection == TurnDirection.Right));
				if (left.Any())
				{
					bullet.Direction = TurnDirection.Right;
					bullet.IsConfident = true;
					foreach (var next in GetCurTurnTilesOnSide(bullet.Location.Location, Direction.Right, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					{
						GetPredTile(next.Location).Update(Bullet.Char);
					}

					continue;
				}

				var right = GetPrevTurnTilesOnSide(bullet.Location.Location, Direction.Right, 2)
					.TakeWhile(x => x?.IsBulletObstacle == false)
					.Where(x => (x.GameObject == GameObject.Bullet && (x.GameObjectDirection == TurnDirection.Left || x.GameObjectDirection == TurnDirection.None)) || ((x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot || x.GameObject == GameObject.PlayerTank) && x.GameObjectDirection == TurnDirection.Left));
				//.Where(x => x.GameObject != GameObject.None && x.GameObject != GameObject.Bang && x.GameObject != GameObject.Bonus && x.GameObjectDirection == TurnDirection.Left);
				if (right.Any())
				{
					bullet.Direction = TurnDirection.Left;
					bullet.IsConfident = true;
					foreach (var next in GetCurTurnTilesOnSide(bullet.Location.Location, Direction.Left, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					{
						GetPredTile(next.Location).Update(Bullet.Char);
					}

					continue;
				}

				var up = GetPrevTurnTilesOnSide(bullet.Location.Location, Direction.Up, 2)
					.TakeWhile(x => x?.IsBulletObstacle == false)
					.Where(x => (x.GameObject == GameObject.Bullet && (x.GameObjectDirection == TurnDirection.Down || x.GameObjectDirection == TurnDirection.None)) || ((x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot || x.GameObject == GameObject.PlayerTank) && x.GameObjectDirection == TurnDirection.Down));
				//.Where(x => x.GameObject != GameObject.None && x.GameObject != GameObject.Bang && x.GameObject != GameObject.Bonus && x.GameObjectDirection == TurnDirection.Down);
				if (up.Any())
				{
					bullet.Direction = TurnDirection.Down;
					bullet.IsConfident = true;
					foreach (var next in GetCurTurnTilesOnSide(bullet.Location.Location, Direction.Down, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					{
						GetPredTile(next.Location).Update(Bullet.Char);
					}

					continue;
				}

				var down = GetPrevTurnTilesOnSide(bullet.Location.Location, Direction.Down, 2)
					.TakeWhile(x => x?.IsBulletObstacle == false)
					.Where(x => (x.GameObject == GameObject.Bullet && (x.GameObjectDirection == TurnDirection.Up || x.GameObjectDirection == TurnDirection.None)) || ((x.GameObject == GameObject.AiBot || x.GameObject == GameObject.PlayerBot || x.GameObject == GameObject.PlayerTank) && x.GameObjectDirection == TurnDirection.Up));
				//.Where(x => x.GameObject != GameObject.None && x.GameObject != GameObject.Bang && x.GameObject != GameObject.Bonus && x.GameObjectDirection == TurnDirection.Up);
				if (down.Any())
				{
					bullet.Direction = TurnDirection.Up;
					bullet.IsConfident = true;
					foreach (var next in GetCurTurnTilesOnSide(bullet.Location.Location, Direction.Up, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					{
						GetPredTile(next.Location).Update(Bullet.Char);
					}

					continue;
				}
			}
			foreach(var enemy in GetEnemies())
			{
				var possibleShoots = GetCurTurnTilesOnSide(enemy.Location.Location, Direction.Left, 2).TakeWhile(x => x?.IsBulletObstacle == false)
					.Concat(GetCurTurnTilesOnSide(enemy.Location.Location, Direction.Right, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					.Concat(GetCurTurnTilesOnSide(enemy.Location.Location, Direction.Up, 2).TakeWhile(x => x?.IsBulletObstacle == false))
					.Concat(GetCurTurnTilesOnSide(enemy.Location.Location, Direction.Down, 2).TakeWhile(x => x?.IsBulletObstacle == false));

				foreach(var pos in possibleShoots)
				{
					GetPredTile(pos.Location).Update(Bullet.Char);
				}
			}
		}

		public List<Tile> GetCurTurnTilesOnSide(Point center, Direction dir, int radius)
		{
			var result = new List<Tile>();
			if (dir == Direction.Left)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetTile(center.ShiftLeft(i)));
				}
			}
			if (dir == Direction.Right)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetTile(center.ShiftRight(i)));
				}
			}
			if (dir == Direction.Up)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetTile(center.ShiftTop(i)));
				}
			}
			if (dir == Direction.Down)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetTile(center.ShiftBottom(i)));
				}
			}

			return result;
		}

		public bool CheckNextTurnBulletHit(Point point)
		{
			return GetPredTile(point).GameObject == GameObject.Bullet;
		}

		public List<Tile> GetPrevTurnTilesOnSide(Point center, Direction dir, int radius)
		{
			var result = new List<Tile>();
			if (dir == Direction.Left) 
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetPrevTurnTile(center.ShiftLeft(i)));
				}
			}
			if (dir == Direction.Right)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetPrevTurnTile(center.ShiftRight(i)));
				}
			}
			if (dir == Direction.Up)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetPrevTurnTile(center.ShiftTop(i)));
				}
			}
			if (dir == Direction.Down)
			{
				for (int i = 1; i <= radius; i++)
				{
					result.Add(GetPrevTurnTile(center.ShiftBottom(i)));
				}
			}

			return result;
		}

		public List<Tile> GetNeighbors(Tile origin)
		{
			var loc = origin.Location;
			var neightborsPoints = loc.Neighbors();
			var result = new List<Tile>();
			foreach(var n in neightborsPoints)
			{
				result.Add(CurrTurnMap[n.Y, n.X]);
			}

			return result;
		}

		public string CurrTurnMapToString()
		{
			var str = new char[Size * Size + Size];
			for (int i = 0; i < Size; i++)
			{
				int j = 0;
				for (; j < Size; j++)
				{
					str[i * Size + j + i] = CurrTurnMap[i, j].OriginalChar;
				}

				str[i * Size + j + i] = '\n';
			}

			return new string(str);
		}

		public string GridToString()
		{
			const int sep = 2;
			var builder = new List<string>(Size);
			for (int i = 0; i < Size; i++)
			{
				int j = 0;
				var str = new char[Size + sep + Size + sep + Size];
				for (; j < Size; j++)
				{
					str[j] = PrevTurnMap[i, j].OriginalChar;
				}

				for (; j < Size + sep; j++)
				{
					str[j] = ' ';
				}

				for (int z = 0; z < Size; z++)
				{
					str[j] = CurrTurnMap[i, z].OriginalChar;
					j++;
				}

				for (; j < Size + sep; j++)
				{
					str[j] = ' ';
				}

				for (int z = 0; z < Size; z++)
				{
					str[j] = PredictMap[i, z].OriginalChar;
					j++;
				}

				builder.Add(new string(str));
			}

			builder.Reverse();
			return string.Join("\n", builder);
		}


		public List<Tank> GetAiBots()
		{
			return AiBots.ToList();
		}

		public List<Tank> GetPlayerBots()
		{
			return PlayerBots.ToList();
		}

		public List<Tank> GetEnemies()
		{
			return Tanks.Where(x => x.Type != TankType.Player).ToList();
		}

		public List<Bullet> GetBullets()
		{
			return Bullets.ToList();
		}


		public List<Tank> GetPrevTurnAiBots()
		{
			return PrevTurnAiBots.ToList();
		}

		public List<Tank> GetPrevTurnPlayerBots()
		{
			return PrevTurnPlayerBots.ToList();
		}

		public List<Tank> GetPrevTurnEnemies()
		{
			return PrevTurnTanks.Where(x => x.Type != TankType.Player).ToList();
		}

		public List<Bullet> GetPrevTurnBullets()
		{
			return PrevTurnBullets.ToList();
		}


		public Tank GetPlayer()
		{
			return Player;
		}
		public Tile GetTile(int x, int y)
		{
			return GetTileFromMap(CurrTurnMap, x, y);
		}
		public Tile GetTile(Point point)
		{
			return GetTileFromMap(CurrTurnMap, point.X, point.Y);
		}
		public Tile GetTile(Tile tile, Direction dir)
		{
			if (dir == Direction.Down)
			{
				var coord = tile.Location.ShiftBottom();
				if (coord.X < 0 || coord.Y < 0) return null;
				return GetTile(coord);
			}
			if (dir == Direction.Up)
			{
				var coord = tile.Location.ShiftTop();
				if (coord.X < 0 || coord.Y < 0) return null;
				return GetTile(coord);
			}
			if (dir == Direction.Left)
			{
				var coord = tile.Location.ShiftLeft();
				if (coord.X < 0 || coord.Y < 0) return null;
				return GetTile(coord);
			}
			if (dir == Direction.Right)
			{
				var coord = tile.Location.ShiftRight();
				if (coord.X < 0 || coord.Y < 0) return null;
				return GetTile(coord);
			}

			return null;
		}

		public Tile[,] GetPlayerWindow()
		{
			return GetWindow(3, Player.Location);
		}

		public Tile[,] GetWindow(int radius, Tile center)
		{
			var length = 2 * radius + 1;
			var window = new Tile[length, length];
			for(int i = 0; i < length; i++)
			{
				var m = center.Location.X + i - radius;
				for(int j = 0; j < length; j++)
				{
					var n = center.Location.Y + j - radius;
					window[i, j] = CurrTurnMap[m,n];
				}
			}

			return window;
		}

		public Tank GetTankFromPrevLocation(Tile tile)
		{
			return PrevTurnTanks.FirstOrDefault(x => x.Location.Location.Equals(tile.Location));
		}

		public Tile GetPrevTurnTile(int x, int y)
		{
			return GetTileFromMap(PrevTurnMap, x, y);
		}
		public Tile GetPrevTurnTile(Point point)
		{
			return GetTileFromMap(PrevTurnMap, point.X, point.Y);
		}
		public Tile GetPredTile(int x, int y)
		{
			return GetTileFromMap(PredictMap, x, y);
		}
		public Tile GetPredTile(Point point)
		{
			return GetTileFromMap(PredictMap, point.X, point.Y);
		}

		private Tile GetTileFromMap(Tile[,] map, int x, int y)
		{
			if (x < 0 || y < 0 || x >= Size || y >= Size)
				return null;

			return map[y, x];
		}
	}
}

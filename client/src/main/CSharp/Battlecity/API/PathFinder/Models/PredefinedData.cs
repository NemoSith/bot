﻿using API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.PathFinder.Models
{
	public static class PredefinedData
	{
		public static SurfaceBasicMoveWeights SurfaceBasicMoveWeights = new SurfaceBasicMoveWeights
		{
			StrongWall = int.MaxValue,
			Ground = 50,
			Ice = 150,
			Trees = 500,
			Wall = 10000,
			WallBroken1 = 10000,
			WallBroken2 = 10000,
			Water = 10000
		};
	}
}
